const Sequelize = require('sequelize');
const db = require('../config/database');

const Fichas = db.define('fichas', {
    ficha:{
        type: Sequelize.BOOLEAN
    },
    numero_documento:{
        type: Sequelize.INTEGER
    },
    nombre:{
        type: Sequelize.TEXT
    },
    apellido:{
        type: Sequelize.TEXT
    },
    edad:{
        type: Sequelize.INTEGER
    },
    consultas:{
        type: Sequelize.TEXT
    },
    alergias:{
        type: Sequelize.TEXT
    },
    tipo_sangre:{
        type: Sequelize.TEXT
    },
    vacunas:{
        type: Sequelize.TEXT
    },
    tratamiento:{
        type: Sequelize.TEXT
    },
    enfermedad:{
        type: Sequelize.TEXT
    }
})

module.exports = Fichas