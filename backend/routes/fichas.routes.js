const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Fichas = require('../models/fichas');


//Obtener lista de fichas
router.get('/' , (req,res)=> 
Fichas.findAll()
    .then(fichas => {
        res.send(fichas)
    })
    .catch(err=>console.log(err))
);
//Obtener ficha específica
router.get("/:id", async(req,resp)=>{
    try{
        const allFic = await Fichas.findAll({where:req.params});
        resp.send(allFic[0]);
    }catch(error){
        resp.status(400).send("Error");
    }
});
//Actualizar ficha. Con esto podemos ver si el paciente tiene ficha o no y actualizarla si es necesario.
router.post("/agregarFicha/:rut/:numero_documento/:nombre/:apellido/:edad",async(req,resp)=>{
    try{
        const userValid = await Fichas.findOne({
            where: {
              id: req.params.rut,
              numero_documento: req.params.numero_documento,
            },
          });
          if(userValid){
              console.log("hola")
            const user = await Fichas.update(
                {ficha: true,
                nombre: req.params.nombre,
                apellido: req.params.apellido,
                edad: req.params.edad},
                {where:{
                    id:req.params.rut,
                    numero_documento: req.params.numero_documento
                }}
              );
              return resp.send(user);
          }
          else{
            return resp.send("no");
          }
          
          
    }catch(error){
        resp.status(400).send(error.message + error);
    }
});



module.exports = router;