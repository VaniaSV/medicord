const express = require('express');
const router = express.Router();
const db = require('../config/database');
const Doctor = require('../models/doctor');



//Obtener lista de doctores
router.get('/' , (req,res)=> 
Doctor.findAll()
    .then(doctores => {
        res.send(doctores)
    })
    .catch(err=>console.log(err))
);
//Obtener doctor específico
router.get("/:id", async(req,resp)=>{
    try{
        const allDoc = await Doctor.findAll({where:req.params});
        resp.send(allDoc);
    }catch(error){
        resp.status(400).send("Error");
    }
});

router.post("/login/:rut/:clave", async (req, res) => {
    try {
      const user = await Doctor.findOne({
        where: {
          id: req.params.rut,
          contrasena: req.params.clave
        },
      });
      if (!user) return res.status(400).send("usuario o contrasena equivocada");
      else return res.status(200).send("Estas logeado");
    } catch (error) {
      return res.status(400).send(error);
    }
  });

module.exports = router;