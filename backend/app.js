const express = require('express');
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');
const path = require('path');
const { get } = require('http');
var bodyParser = require('body-parser');
// BD
const db = require('./config/database')



//Prueba DB
db.authenticate()
.then(()=>console.log("database connected"))
.catch(err=>console.log("Error: "+ err))

const app = express();

//Rutas
app.get('/', (req, res)=>res.send('INDEX'));
app.use('/doctor', require('./routes/doctor.routes'));
app.use('/fichas', require('./routes/fichas.routes'));


//BP
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());



const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log('Server started on port 5000'));