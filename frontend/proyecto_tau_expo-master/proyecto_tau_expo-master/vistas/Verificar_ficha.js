import * as React from 'react';
import { View, Text, Button, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

/*
EN ESTE ARCHIVO SE VERIFICA EL RUT SI TIENE O NO FICHA MEDICA
SUPOSICION: EN LA BD EXISTEN TODAS LAS PERSONAS,  POR LO QUE SE VERIFICA SOLAMENTE SI TIENEN O NO TIENEN FICHA MEDICA!
*/
import { useEffect, useState } from 'react'
import axios from 'axios'

const Verificar_ficha = ({navigation, route}) => {
    const rut = route.params.rut;
    console.log("el rut es ",rut)
    

//Hooks para la conexion
const [Facts1Data, setFacts1Data] = useState([])
const [loaded, setDataLoaded] = useState(false)
let url = 'http://192.168.1.87:5000/fichas/'+rut
const[link, setLink]=useState(url)
//Requerimientos a la API
useEffect(() => {
  const fetchData = async () => {
      try{
          if (!loaded) {
              const result = await axios.get(link)
              if (result.data) {
                  setDataLoaded(true)
                  setFacts1Data(result.data)
                  
              }
          }
      }
      catch(error){
          if(error.response){
              console.log(error.response.data)
              console.log(error.response.status)
              console.log(error.response.headers)
          }
          else if(error.resquest){
              console.log(error.request)
          }
          else{
              console.log('Error', error.message )
          }
      }
  }
  fetchData()
})
console.log(Facts1Data)


if(Facts1Data.ficha){
  return(
    <View style={styles.container}>
    
    <TouchableOpacity>
                <Image 
                    style={styles.stretch} source={require('../Imagen/check_1.png')}
                >
                    </Image>
            </TouchableOpacity>
            <Text style={styles.texto}>El rut es válido</Text>
    <Text style={styles.texto}>{rut}</Text>
    
    
    <Button
            title="Cambiar ficha medica"
            onPress={() =>{
              navigation.navigate('Crear ficha', {rut: rut})
            } }
          />
          <Button
      title="Volver al menu"
      onPress={() => navigation.navigate('Principal')}

    />
  </View>
  
  )
}

    else{
      return(
        <View style={styles.container}>
          <Text style={styles.texto}>No existe ficha medica.</Text>
          
          <Image 
            style={styles.stretch} source={require('../Imagen/free-sad-face.png')}
          ></Image>
          
          <Button
            title="Ir a crear una ficha medica"
            onPress={() =>{
              navigation.navigate('Crear ficha', {rut: rut})
            } }
          />
          <Button
      title="Volver al menu"
      onPress={() => navigation.navigate('Principal')}

    />

        </View>
  
      );
    } 
}


const styles =StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        justifyContent: 'center',
    },
    stretch: {
      width: 200,
      height: 200,
      resizeMode: 'stretch',
      justifyContent: 'center',
    },
    texto:{
      fontWeight: 'bold',
      fontSize: 20,
    },
  
})

export default Verificar_ficha