import * as React from 'react';
import { View, Text, Button, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';
import { useState } from 'react';

//ESTE ARCHIVO ES EL LOGIN DEL DOCTOR.

const Login_doctor = ({navigation}) => {
  const [ Rut_usuario, onChangeUsuario] = useState("");
  const [Contrasena, onChangeContrasena] = useState("");
  
    return(
      <View style={styles.container}>
        <TouchableOpacity>
                <Image 
                    style={styles.stretch} source={require('../Imagen/doctor.png')}
                >
                    </Image>
            </TouchableOpacity>
        <Text></Text>
        
        <Text>Ingrese su rut:</Text>
          <TextInput
              style={styles.input}
              placeholder = "123456789"
              onChangeText={onChangeUsuario}
              value={Rut_usuario}
          />
          
          <Text>Ingrese su contraseña</Text>
          <TextInput secureTextEntry={true}
              style={styles.input}
              placeholder = "Password"
              onChangeText={onChangeContrasena}
              value={Contrasena}
          />
        
        <Button
          title="Enviar"
          onPress={() => navigation.navigate('Ver ficha paciente',{rut:Rut_usuario, contra: Contrasena})}
        />
        
        
      </View>
    );
}
const styles =StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    input: {
      justifyContent: 'center',
      margin: 30,
      textAlign: 'center',
      height: 50,
      width:200,
      borderColor: '#FF5722',
      borderRadius: 20,
      backgroundColor: '#FFFFFF'
    },
    stretch:{
      width: 300,
      height: 300,
      resizeMode: 'stretch',
    }
})

export default Login_doctor