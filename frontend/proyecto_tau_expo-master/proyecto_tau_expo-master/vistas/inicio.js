import * as React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';

//ESTE ARCHIVO ES SOLO PARA MOSTRAR LA IMAGEN EN EL MENU PRINCIPAL. 
//ESTE ARCHIVO SE IMPORTA EN EL ARCHIVO "Pagina_principal.js".

export default function InicioPag() {
    return (
        <View>
            <TouchableOpacity>
                <Image 
                    style={styles.stretch} source={require('../Imagen/logo.png')}
                >
                    </Image>
            </TouchableOpacity>
            
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
      paddingTop: 50,
      
    },
    stretch: {
      width: 300,
      height: 300,
      resizeMode: 'stretch',
    },
  });
 