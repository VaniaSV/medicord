import * as React from 'react';
import { View, Text, Button, StyleSheet, TextInput, Image } from 'react-native';
import { useState } from 'react';
import InicioPag from './inicio.js'

//ESTE ES EL MENU PRINCIPAL, DONDE SE PUEDE NAVEGAR HACIA EL INICIO DE SESION O HACIA EL VERIFICAR SI UN RUT TIENE FICHA MEDICA. 

const Inicio = ({navigation}) => {
    const [text, onChangeText] = useState("");
    return (
      <View style={styles.container}>
        <InicioPag/>
        <Text style={styles.text}>Ingrese su rut:</Text>
            <TextInput
                style={styles.input}
                placeholder = "Sin puntos ni guion"
                onChangeText={onChangeText}
                value={text}
            />
        <Button
          style={styles.aStyle}
          title="Verificar ficha"
          onPress={() => navigation.navigate('Verificar ficha', {rut: text })}
        />
        
        <Text> </Text>

        <Button
          title="Inicio sesion doctor "
          onPress={() => navigation.navigate('Login doctor')}
        />
        
      </View>
    );
}
const styles = StyleSheet.create({
  aStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#FF9B00",
    borderWidth: 0.5,
    borderColor: "#fff",
    height: 40,
    width: 220,
    borderRadius: 5,
    margin: 5
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 0.5,
    borderColor: "#fff",
    backgroundColor: '#70ccf4',
    flex: 1,
  },
  input: {
    justifyContent: 'center',
    margin: 30,
    textAlign: 'center',
    height: 50,
    width:200,
    borderColor: '#FF5722',
    borderRadius: 20,
    backgroundColor: '#FFFFFF'
  },
  text: {
    height: 50,
    width:200,
    textAlign: 'center',
    justifyContent: 'center',
  },
}) 
  export default Inicio 