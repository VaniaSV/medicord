import * as React from 'react';
import { View, Text, Button, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';
import { useState, useEffect } from 'react';
import axios from 'axios'

//EN ESTE ARCHIVO SE INGRESA EL RUT DE UN PACIENTE PARA DESPUES BUSCAR SU FICHA MEDICA EN LA BASE DE DATOS.

const Ver_ficha_paciente = ({navigation, route}) => {
  const Rut_login = route.params.rut
  const Contra_login = route.params.contra
  const [Rut_paciente, onChangeRut_paciente] = useState("");


//Hooks para la conexion
const [Data, setData] = useState([])
const [loaded, setDataLoaded] = useState(false)
let url = 'http://192.168.1.87:5000/doctor/login/'+Rut_login+'/'+Contra_login
const[link, setLink]=useState(url)
//Requerimientos a la API
useEffect(() => {
  const fetchData = async () => {
      try{
          if (!loaded) {
              const result = await axios.post(link)
              if (result.data) {
                  setDataLoaded(true)
                  setData(result.data)
                  
              }
          }
      }
      catch(error){
          if(error.response){
              console.log(error.response.data)
              console.log(error.response.status)
              console.log(error.response.headers)
          }
          else if(error.resquest){
              console.log(error.request)
          }
          else{
              console.log('Error', error.message )
          }
      }
      
  }
  fetchData()
})
console.log(Data)


//.................


  if(Data == "Estas logeado"){
    return(
      <View style={styles.container}>
        <TouchableOpacity>
                <Image 
                    style={styles.stretch} source={require('../Imagen/medical-report.png')}
                >
                    </Image>
            </TouchableOpacity>
        <Text></Text>
          <Text>Bienvenido</Text>
          <Text></Text>
        
          <Text>Ingrese el rut del paciente:</Text>
            <TextInput
                style={styles.input}
                placeholder = "Sin punto ni guión"
                onChangeText={onChangeRut_paciente}
                value={Rut_paciente}
            />
          <Button
            title="Ver ficha medica"
            onPress={() => navigation.navigate('Ficha medica rut', {rut:Rut_paciente})}
          />
      </View>
    );
  }else{
    return(
      <View style={styles.container}>
        <Text>Inicio incorrecto :(</Text>
        <Button
        title="Volver al Menu"
        onPress={() => navigation.navigate('Principal')}
        />
      </View>
    );
  }

    
}
const styles =StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    input: {
      justifyContent: 'center',
      margin: 30,
      textAlign: 'center',
      height: 50,
      width:200,
      borderColor: '#FF5722',
      borderRadius: 20,
      backgroundColor: '#FFFFFF'
    },
    stretch:{
      width: 300,
      height: 300,
      resizeMode: 'stretch',
      
    }
})

export default Ver_ficha_paciente