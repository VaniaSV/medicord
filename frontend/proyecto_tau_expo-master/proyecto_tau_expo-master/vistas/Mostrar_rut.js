import * as React from 'react';
import { View, Text, Button, StyleSheet } from 'react-native';

//CUANDO SE VERIFIQUE QUE UN RUT SI TIENE FICHA MEDICA, ESTE ARCHIVO MOSTRARA EL RUT DEL PACIENTE PARA QUE UN DOCTOR PUEDA INGRESAR SU RUT Y VER SU RESPECTIVA FICHA MEDICA.

const Mostrar_rut = ({navigation, route}) => {
  const rut = route.params.rut;
  return(
    <View style={styles.container}>
      <Text>Se ha creado la ficha para</Text>
      <Text>Rut : {rut}</Text>
      <Button
        title="Volver al Menu"
        onPress={() => navigation.navigate('Principal')}

      />
    </View>
  );
}
const styles =StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    }
})

export default Mostrar_rut