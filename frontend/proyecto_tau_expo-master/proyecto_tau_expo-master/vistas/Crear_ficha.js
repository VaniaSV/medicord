import * as React from 'react';
import { View, Text, Button, StyleSheet, TextInput, Image, TouchableOpacity } from 'react-native';
import { useState, useEffect } from 'react';
import axios from 'axios'

//EN ESTE ARCHIVO SE CREA UNA FICHA MEDICA PARA UN PACIENTE CUANDO ESTE NO TIENE.

const Crear_ficha = ({navigation, route}) => {
  const Rut = route.params.rut;
  const [Nombres, onChangeNombres] = useState("");
  const [Apellidos, onChangeApellidos] = useState("");
  const [Edad, onChangeEdad] = useState("");
  const [Num_documento, onChangeNum_documento] = useState("");
  const [Facts2Data, setFacts2Data] = useState([])
  const [loaded, setDataLoaded] = useState(true)
  
  
 
  
  console.log(Nombres)
  console.log(Apellidos)
  console.log(Num_documento)
  console.log(Edad)
  console.log(Rut)
  //Hooks para la conexion
  console.log('http://192.168.1.87:5000/fichas/agregarFicha/'+Rut+'/'+Num_documento+'/'+Nombres+'/'+Apellidos+'/'+Edad)

  //Requerimientos a la API
  useEffect(() => {
    const fetchData = async () => {
        try{
            if (!loaded) {
                const result = await axios.post('http://192.168.1.87:5000/fichas/agregarFicha/'+Rut+'/'+Num_documento+'/'+Nombres+'/'+Apellidos+'/'+Edad)
                if (result.data) {
                    setDataLoaded(true)
                    setFacts2Data(result.data)
                }
            }
        }
        catch(error){
            if(error.response){
                console.log(error.response.data)
                console.log(error.response.status)
                console.log(error.response.headers)
            }
            else if(error.resquest){
                console.log(error.request)
            }
            else{
                console.log('Error', error.message )
            }
        }
        
    }
    fetchData()
  })
console.log(Facts2Data)

  
  return(
    <View>
          <TouchableOpacity>
                <Image 
                    style={styles.stretch} source={require('../Imagen/clipboard.png')}
                >
                    </Image>
            </TouchableOpacity>
          <Text>Ingrese todos sus nombres</Text>
          <TextInput
              style={styles.input}
              placeholder = "Juan"
              onChangeText={onChangeNombres}
              value={Nombres}
          />
          
          {/*Apellidos*/}
          <Text>Ingrese todos sus apellidos</Text>
          <TextInput
              style={styles.input}
              placeholder = "Alvear"
              onChangeText={onChangeApellidos}
              value={Apellidos}
          />

          {/*Numero de documento*/}
          <Text>Ingrese su número de documento</Text>
          <TextInput
              style={styles.input}
              placeholder = "Omita los puntos"
              onChangeText={onChangeNum_documento}
              value={Num_documento}
          />

          {/*Edad*/}
          <Text>Ingrese su edad</Text>
          <TextInput
              style={styles.input}
              placeholder = "99"
              onChangeText={onChangeEdad}
              value={Edad}
          />
          
          <Button
            title="Generar Ficha"
            onPress={
              () => {
                setDataLoaded(false)
                navigation.navigate('Mostrar rut',{rut: Rut})
               
              }}
              />

        </View>
    );
  }

  

  
const styles =StyleSheet.create({
  
  input: {
      alignItems: 'center',
      height: 40,
      margin: 12,
      borderWidth: 1,
      justifyContent: 'center',
  },
  stretch: {
    flexDirection: 'column',
    width: 300,
    height: 300,
    resizeMode: 'stretch',
    justifyContent: 'center',
    alignSelf: 'center'
    
  }
})


export default Crear_ficha