import * as React from 'react';
import { View, Text, Button, StyleSheet, TextInput, Image } from 'react-native';
import { useState, useEffect } from 'react';
import axios from 'axios'

//ESTE ARCHIVO MUESTA LOS DATOS DE LA FICHA MEDICA ASOCIADA A UN PACIENTE.

const Ficha_medica_rut = ({navigation, route}) => {
const rut = route.params.rut

//Hooks para la conexion
const [Data, setData] = useState([])
const [loaded, setDataLoaded] = useState(false)
let url = 'http://192.168.1.87:5000/fichas/'+rut
const[link, setLink]=useState(url)
//Requerimientos a la API
useEffect(() => {
  const fetchData = async () => {
      try{
          if (!loaded) {
              const result = await axios.get(link)
              if (result.data) {
                  setDataLoaded(true)
                  setData(result.data) 
              }
          }
      }catch(error){
          if(error.response){
              console.log(error.response.data)
              console.log(error.response.status)
              console.log(error.response.headers)
          }
          else if(error.resquest){
              console.log(error.request)
          }
          else{
              console.log('Error', error.message )
          }
      }   
  }
  fetchData()
})
console.log(Data)


//..............................

    if(Data.id){
      return(
        <View style={styles.container}>
          <Text style={flattenStyle}> Ficha medica de: </Text>
          <Text style={styles.text_1} > {Data.nombre + " " +Data.apellido} </Text>
          <Text>Rut: {Data.id} </Text>
          <Text>Edad: {Data.edad} </Text>
          <Text>Consultas: {Data.consultas} </Text>
          <Text>Alergias: {Data.alergias} </Text>
          <Text>Tipo de sangre: {Data.tipo_sangre} </Text>
          <Text>Vacunas: {Data.vacunas} </Text>
          <Text>Tratamientos: {Data.tratamiento} </Text>
          <Text>Enfermedades: {Data.enfermedad} </Text>
  
        <Button
        style= {styles.submitButton}
        title="Buscar otro paciente"
        onPress={() => navigation.navigate('Ver ficha paciente')}
        />
  
        <Text> </Text>
  
        <Button
        style= {styles.submitButton}
        title="Volver al menu"
        onPress={() => navigation.navigate('Principal')}
        />
        </View> 
      );
    }
    else{
      return(
        <View style={styles.container}>
          <Image 
            style={styles.stretch} source={require('../Imagen/free-sad-face.png')}
          ></Image>
          <Text style={flattenStyle}>{ "No se ha encontrado información de " + rut }</Text>
          
          
  
        <Button
        title="Intentar nuevamente"
        onPress={() => navigation.navigate('Ver ficha paciente')}
        />
  
        <Text> </Text>
  
        <Button
        title="Volver al menu"
        onPress={() => navigation.navigate('Principal')}
        />
        </View> 
      );
    }
    
}
const styles =StyleSheet.create({
    container: { flex: 1, alignItems: 'center', justifyContent: 'center' },
    footer: {alignItems: 'center', textDecorationLine: 'underline'},
    submitButton: {
      position: 'absolute',
      bottom:10,
      left:10
    },
    input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
    },
    input: {
      justifyContent: 'center',
      margin: 30,
      textAlign: 'center',
      height: 50,
      width:200,
      borderColor: '#FF5722',
      borderRadius: 20,
      backgroundColor: '#70ccf4'
    },
    text: {
      color: "#000",
      fontSize: 14,
      fontWeight: "bold",
      fontFamily: 'sans-serif',
      justifyContent: 'center',
      alignItems: 'center',

    },
    text_1:{
      color: "#7A7A7A",
      fontSize: 25,

    },
    header: {
      color: "#000000",
      fontSize: 30,
    },
    stretch:{
      width: 300,
      height: 300,
      resizeMode: 'stretch',
    }
})

const flattenStyle = StyleSheet.flatten([
  styles.text,
  styles.header
]);

export default Ficha_medica_rut