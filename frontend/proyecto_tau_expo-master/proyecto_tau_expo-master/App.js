// In App.js in a new project

import * as React from 'react';
import {StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Inicio from './vistas/Pagina_principal';
import Verificar_ficha from './vistas/Verificar_ficha';
import Mostrar_rut from './vistas/Mostrar_rut';
import Login_doctor from './vistas/Login_doctor';
import Crear_ficha from './vistas/Crear_ficha'
import Ver_ficha_paciente from './vistas/Ver_ficha_paciente'
import Ficha_medica_rut from './vistas/Ficha_medica_rut'


//EN ESTE ARCHIVO SOLO ESTA LO NECESARIO PARA PODER NAVEGAR ENTRE LAS PAGINAS.


const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={"Principal"}>
        <Stack.Screen name="Principal" component={Inicio} />
        <Stack.Screen name="Verificar ficha" component={Verificar_ficha} />
        <Stack.Screen name="Mostrar rut" component={Mostrar_rut} />
        <Stack.Screen name="Login doctor" component={Login_doctor} />
        <Stack.Screen name="Crear ficha" component={Crear_ficha} />
        <Stack.Screen name="Ver ficha paciente" component={Ver_ficha_paciente} />
        <Stack.Screen name="Ficha medica rut" component={Ficha_medica_rut} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles =StyleSheet.create({
  container: { flex: 1, alignItems: 'center', justifyContent: 'center' }

})

export default App;